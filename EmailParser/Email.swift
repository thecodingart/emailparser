//
//  Email.swift
//  EmailParser
//
//  Created by Brandon Levasseur on 10/18/14.
//  Copyright (c) 2014 Brandon Levasseur. All rights reserved.
//

import Foundation

class Email {
    var from: String!
    var sender: String? = nil
    var replyTo: String? = nil
    var to: String!
    var cc: String? = nil
    var bcc: String? = nil
    var subject: String? = nil
    var comments: String? = nil
    var keywords: String? = nil
    var originalDate: NSDate? = nil
    /**
    Identification fields
**/
    var messageId: String? = nil
    var inReplyTo: String? = nil
    var references: String? = nil
    var msgId: String? = nil
    var idLeft: String? = nil
    var idRight: String? = nil
    
    var bodyContent = [BodyContent]()
    
    init() {
        
    }

}
