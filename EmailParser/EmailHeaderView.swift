//
//  EmailHeaderView.swift
//  EmailParser
//
//  Created by Brandon Levasseur on 10/26/14.
//  Copyright (c) 2014 Brandon Levasseur. All rights reserved.
//

import UIKit

class EmailHeaderView: UIView {
    @IBOutlet weak var toLabel: UILabel!
    @IBOutlet weak var fromLabel: UILabel!
}
