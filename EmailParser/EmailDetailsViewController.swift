//
//  EmailDetailsViewController.swift
//  EmailParser
//
//  Created by Brandon Levasseur on 10/19/14.
//  Copyright (c) 2014 Brandon Levasseur. All rights reserved.
//

import UIKit

class EmailDetailsViewController: UIViewController, UITableViewDataSource {
    
    @IBOutlet weak var tableView: UITableView!
    @IBOutlet weak var headerView: EmailHeaderView!
    @IBOutlet weak var bodyLabel: UILabel!
    var email: Email? {
        didSet {
            // Update the view.
            self.tableView?.reloadData()
        }
    }
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        headerView.toLabel.text = "To: \(email?.to)"
        headerView.fromLabel.text = "From: \(email?.from)"
        tableView.rowHeight = UITableViewAutomaticDimension
        tableView.estimatedRowHeight = 100;
        self.tableView.reloadData()
        // Do any additional setup after loading the view.
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    
    func numberOfSectionsInTableView(tableView: UITableView) -> Int {
        return 1
    }
    
    func tableView(tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return email!.bodyContent.count
    }
    
    func tableView(tableView: UITableView, cellForRowAtIndexPath indexPath: NSIndexPath) -> UITableViewCell {
        let cell = cellForRowAtIndexPath(indexPath)
        
        return cell
    }
    
    func cellForRowAtIndexPath(indexPath: NSIndexPath) -> UITableViewCell {
        let bodyContent = email!.bodyContent[indexPath.row]
        let contentType = bodyContent.contentType
        
        switch contentType {
        case contentType where contentType.hasPrefix("text"):
            println("I am text")
            let cell = tableView.dequeueReusableCellWithIdentifier("EmailTextTableViewCell", forIndexPath: indexPath) as EmailTextTableViewCell
            configureTextCell(cell, withBodyContent: bodyContent)
            return cell
        case contentType where contentType.hasPrefix("image"):
            println("I have an image")
            let cell = tableView.dequeueReusableCellWithIdentifier("EmailImageTableViewCell", forIndexPath: indexPath) as EmailImageTableViewCell
            configureImageCell(cell, withBodyContent: bodyContent)
            return cell
        default:
            println("I have no clue what I have")
            
        }
        
        
        return UITableViewCell()
    }
    
    func configureTextCell(cell: EmailTextTableViewCell, withBodyContent bodyContent: BodyContent) {
        var bodyAttribute = NSPlainTextDocumentType
        var contentData = bodyContent.contentData
        let attributedString = NSMutableAttributedString(string: "")
        let contentType = bodyContent.contentType//.stringByReplacingOccurrencesOfString(";", withString: "", options: nil, range: nil)
        switch contentType {
        case contentType where (contentType as NSString).containsString("html"):
            println("I have html text")
            attributedString.appendAttributedString(NSAttributedString(string: "html:\n"))
            bodyAttribute = NSHTMLTextDocumentType
        case contentType where (contentType as NSString).containsString("plain"):
            println("I have plain text")
            attributedString.appendAttributedString(NSAttributedString(string: "plain text:\n"))
        default:
            println("I have no clue what I have")
            
        }
        
        if let transferEncoding = bodyContent.contentTransferEncoding {
            let stringData = NSMutableString(data: contentData, encoding: NSUTF8StringEncoding)!
            switch transferEncoding {
            case "quoted-printable":
                //Let us separate the quoted-printable encoded text
                let regularExpression = NSRegularExpression(pattern: "=([0-9a-fA-F]{2})|=\r\n", options: .AnchorsMatchLines, error: nil)
                let stringRange = NSRange(location: 0, length: stringData.length)
                regularExpression?.replaceMatchesInString(stringData, options: nil, range: stringRange, withTemplate: "")
                contentData = stringData.dataUsingEncoding(NSUTF8StringEncoding)
            default:
                println("We have not handled \(transferEncoding) as a transfer encoding type")
            }
        }
        
        //TODO parse out data encoding here
        var encoding = NSUTF8StringEncoding
        
        let attributedOptions = [NSDocumentTypeDocumentAttribute: bodyAttribute, NSCharacterEncodingDocumentAttribute: NSNumber(unsignedLong: encoding)]
        attributedString.appendAttributedString(NSAttributedString(data: contentData, options: attributedOptions, documentAttributes: nil, error: nil)!)
        cell.emailTextLabel.attributedText = attributedString
    }
    
    func configureImageCell(cell: EmailImageTableViewCell, withBodyContent bodyContent: BodyContent) {
        let image = UIImage(data: bodyContent.contentData)
        cell.emailImageView.image = image
    }
    
}
