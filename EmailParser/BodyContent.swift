//
//  BodyContent.swift
//  EmailParser
//
//  Created by Brandon Levasseur on 10/26/14.
//  Copyright (c) 2014 Brandon Levasseur. All rights reserved.
//

import Foundation

class BodyContent {
    var contentType: String!
    var contentTransferEncoding: String!
    var contentData: NSData!
    
    init() {
        
    }
}