//
//  EmailTextTableViewCell.swift
//  EmailParser
//
//  Created by Brandon Levasseur on 10/26/14.
//  Copyright (c) 2014 Brandon Levasseur. All rights reserved.
//

import UIKit

class EmailTextTableViewCell: UITableViewCell {
    
    @IBOutlet weak var emailTextLabel: UILabel!

}
