//
//  ViewController.swift
//  EmailParser
//
//  Created by Brandon Levasseur on 10/18/14.
//  Copyright (c) 2014 Brandon Levasseur. All rights reserved.
//

import UIKit

class EmailListViewController: UITableViewController {
    
    var emails = [Email]()

    override func awakeFromNib() {
        super.awakeFromNib()
        title = "Emails"
    }
    
    override func viewDidLoad() {
        let getEmailOperation = GetEmailOperation()
        getEmailOperation.successCallback = { emails in
            self.emails = emails
            self.tableView.reloadData()
        }
        
        getEmailOperation.failureCallback = {
            (errorTitle, errorDescription) in
            println("There was an error \(errorTitle) + \(errorDescription)")
        }
        
        getEmailOperation.startParsing()
    }
    
    override func numberOfSectionsInTableView(tableView: UITableView) -> Int {
        return 1
    }
    
    override func tableView(tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return emails.count
    }
    
    override func tableView(tableView: UITableView, cellForRowAtIndexPath indexPath: NSIndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCellWithIdentifier("Cell", forIndexPath: indexPath) as UITableViewCell
        let email = emails[indexPath.row]
        cell.textLabel.text = email.subject
        
        return cell
    }
    
    
    override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
        if segue.identifier == "showEmail" {
            if let indexPath = self.tableView.indexPathForSelectedRow() {
                let destinationViewController = segue.destinationViewController as EmailDetailsViewController
                let email = emails[indexPath.row]
//                println(email.bodyText)
                destinationViewController.email = email
            }
        }
    }

}

