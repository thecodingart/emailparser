//
//  EmailParseOperation.swift
//  EmailParser
//
//  Created by Brandon Levasseur on 10/18/14.
//  Copyright (c) 2014 Brandon Levasseur. All rights reserved.
//

import Foundation
import UIKit

class GetEmailOperation {
    var successCallback: ((emails: [Email]) -> ())?
    var failureCallback: ((errorTitle: String, errorDescription: String) -> ())?
    
    init() {
        
    }
    
    func startParsing() {
        var emails = [Email]()
        var emailFilePaths = [NSString]()
        let filePath1 = NSBundle.mainBundle().pathForResource("TestEmail2", ofType: "eml")
        if let path = filePath1 {
            emailFilePaths.append(path)
        }
        
        let filePath2 = NSBundle.mainBundle().pathForResource("TestEmail", ofType: "eml")
        if let path = filePath2 {
            emailFilePaths.append(path)
        }
        
        for path in emailFilePaths {
            var fileData = NSString(contentsOfFile: path, encoding: NSUTF8StringEncoding, error: nil)!
            //            println(fileData)
            //We are not worried about attached content as this point, just the body and it's content-type
            let (headerString, bodyString) = separateHeaderAndBodyDataFromString(fileData) //fileData.componentsSeparatedByString("\r\n\r\n") as [String]
            let headerData = extractHeaderDataFromString(headerString)
            let bodyContents = extractBodyDataFromString(bodyString)
            
            let email = emailFromHeaderData(headerData, bodyContentData: bodyContents)
            emails.append(email)
        }
        
        if let callback = successCallback {
            //                println(email.bodyText)
            callback(emails: emails)
        }
    }
    
    func separateHeaderAndBodyDataFromString(emailStringData: String) -> (headerString: String, bodyString: String) {
        let emailString = emailStringData as NSString
        var bodyString = ""
        var headerString = ""
        let regularExpression = NSRegularExpression(pattern: "\r?\nMime-Version[\\s\\S]*?\r?\n", options: NSRegularExpressionOptions.CaseInsensitive, error: nil)
        if let result = regularExpression?.firstMatchInString(emailString, options: nil, range: NSRange(location: 0, length: emailString.length)) {
            let resultString = emailString.substringWithRange(result.range)
            let stringComponents = emailString.componentsSeparatedByString(resultString) as [NSString]
            headerString = stringComponents.first!
            bodyString = stringComponents.last!
            
        }
        return (headerString, bodyString)
    }
    
    func extractHeaderDataFromString(stringData: String) -> [String: String] {
        println("stringData: \(stringData)")
        var headerFields = [String: String]()
        let regularExpression = NSRegularExpression(pattern: "^(\\S+):(.*)$", options: NSRegularExpressionOptions.AnchorsMatchLines, error: nil)
        let stringRange = NSRange(location: 0, length: countElements(stringData))
        if let match = regularExpression?.matchesInString(stringData, options: nil, range: stringRange) as? [NSTextCheckingResult] {
            
            for checkingResult in match {
                let checkingRange = checkingResult.range
                
                let headerField = (stringData as NSString).substringWithRange(checkingRange)
                let range = (headerField as NSString).rangeOfString(": ")
                
                let fieldRange = NSRange(location: 0, length: range.location)
                let contentRange = NSRange(location: range.location + range.length, length: checkingRange.length - range.location - range.length)
                //println("\(fieldRange) + \(contentRange) + \(range)")
                let fieldType = (headerField as NSString).substringWithRange(fieldRange)
                let fieldContent = (headerField as NSString).substringWithRange(contentRange)
                headerFields[fieldType] = fieldContent
            }
        }
        return headerFields
    }
    
    
    func extractBodyDataFromString(stringData: String) -> [BodyContent] {
        
        var bodyData = NSMutableString(string: stringData)
        let regularExpression = NSRegularExpression(pattern: "--[^\n\r]*\r?\nContent-[\\s\\S]*?\r?\n\r?\n[\\s\\S]*?", options: NSRegularExpressionOptions.AnchorsMatchLines, error: nil)
        let stringRange = NSRange(location: 0, length: countElements(stringData))
        var bodyHeaders = [String]()
        var bodyContent = [String]()
        
        if let match = regularExpression?.matchesInString(stringData, options: nil, range: stringRange) as? [NSTextCheckingResult] {
            
            if match.count > 0 {
                
                for checkingResult in match {
                    let checkingRange = checkingResult.range
                    
                    let headerField = (stringData as NSString).substringWithRange(checkingRange)
                    bodyHeaders.append(headerField)
                }
            } else {
                let adjustedExpression = NSRegularExpression(pattern: "[\\s\\S]*?Content-Type:[\\s\\S]*?\r?\n\r?\n", options: NSRegularExpressionOptions.AnchorsMatchLines, error: nil)
                
                if let match = adjustedExpression?.matchesInString(stringData, options: nil, range: stringRange) as? [NSTextCheckingResult] {
                    
                    for checkingResult in match {
                        let checkingRange = checkingResult.range
                        
                        let headerField = (stringData as NSString).substringWithRange(checkingRange)
                        bodyHeaders.append(headerField)
                    }
                    
                }
            }
        }
        
        let headerCount = bodyHeaders.count
        
        for (index, header) in enumerate(bodyHeaders) {
            if let range = stringData.rangeOfString(header) {
                if index < (headerCount - 1) {
                    if let nextRange = stringData.rangeOfString(bodyHeaders[index + 1]) {
                        let headerContent = stringData.substringWithRange(Range(start: range.endIndex, end: nextRange.startIndex))
                        bodyContent.append(headerContent)
                    }
                } else {
                    let headerContent = stringData.substringWithRange(Range(start: range.endIndex, end: stringData.endIndex))
                    bodyContent.append(headerContent)
                }
            }
        }
        
        
        let bodyContents = bodyContentFromHeaders(bodyHeaders, andBodyData: bodyContent)
        
        return bodyContents
    }
    
    func bodyContentFromHeaders(headers: [String], andBodyData bodyData: [String]) -> [BodyContent] {
        var bodyContentArray = [BodyContent]()
        for (index, header) in enumerate(headers) {
            let body = BodyContent()
            let headerFields = extractHeaderDataFromString(header)
            body.contentType = headerFields["Content-Type"]
            body.contentTransferEncoding = headerFields["Content-Transfer-Encoding"]
            var stringData = NSMutableString(string: bodyData[index])
            let regex = NSRegularExpression(pattern: "/\r?\n([ \t])/g", options: nil, error: nil)
            regex?.replaceMatchesInString(stringData, options: nil, range: NSRange(location: 0, length: stringData.length), withTemplate: "")
            if body.contentType.hasPrefix("image") {
                let adjustedString = stringData.componentsSeparatedByString("--").first as NSMutableString
                println("adjustedString: \(adjustedString)")
                let imageData = NSData(base64EncodedString: adjustedString, options: NSDataBase64DecodingOptions.IgnoreUnknownCharacters)
                println("base64 data: \(imageData)")
                body.contentData = imageData
            } else {
                body.contentData = bodyData[index].dataUsingEncoding(NSUTF8StringEncoding, allowLossyConversion: true)
            }
            bodyContentArray.append(body)
        }
        
        return bodyContentArray
    }
    
    func emailFromHeaderData(headerData:[String: String], bodyContentData: [BodyContent]) -> Email {
        let email = Email()
        if let dateString = headerData["Date"] {
            let dateFormatter = NSDateFormatter()
            dateFormatter.dateFormat = "EEE, dd MMM yyyy HH:mm:ss ZZZZZ"
            dateFormatter.locale = NSLocale(localeIdentifier: "en_US_POSIX")
            let date = dateFormatter.dateFromString(dateString)
            email.originalDate = date
        }
        
        email.from = headerData["From"]
        email.sender = headerData["Sender"]
        email.replyTo = headerData["reply-to"]
        //TOD: To field has extra text that should be parsed out
        if let toAddress = headerData["To"] {
            email.to = toAddress
        } else if let toAddress = headerData["Delivered-To"] {
            email.to = headerData["Delivered-To"]
        }
        email.cc = headerData["cc"]
        email.bcc = headerData["bcc"]
        email.messageId = headerData["message-id"]
        email.inReplyTo = headerData["In-Reply-To"]
        email.references = headerData["References"]
        //Ignoring parsing out message id, id-left, id-right, no fold literal as we are not constructing email chains and such
        email.subject = headerData["Subject"]
        email.comments = headerData["Comments"]
        email.keywords = headerData["Keywords"]
        
        email.bodyContent = bodyContentData
        return email
    }
}

